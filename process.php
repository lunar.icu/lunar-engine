<?php
$route->data(get_included_files());
$route->data($_POST);
$arrPost = $_POST;
end($arrPost);
$post = key($arrPost);
if(isset($post) || $post != null) {
    $post();
}

function login() {
    global $session, $route;
    $loginType = 'Login'.ucfirst($_POST['LoginType']);
    $authName = $_POST['authName'];
    $password = $_POST['password'];
    $lt = new $loginType();
    $result = $lt->loginCheck($authName, $password);
    if ($result) {
        $route->data($result);
        $session->add('lang', $result['lang']);
        $session->add('uid', $result['uid']);
        $session->printAll();
    }

}
