<?php

class LoginNormal extends Database
{

    private $table;
    private $passwordTable;
    private $authTable;
    private $options;

    public function __construct()
    {
        parent::__construct();
        $config = new Config();
        print_r($config->login);
        $data = (object) $config->login['normal'];
        $this->table = $data->table;
        $this->passwordTable = $data->passwordTable;
        $this->authTable = $data->authTable;
        $this->options = $data->encryptOptions;


    }

    public function loginCheck($authName, $password) {
        $data =  $this->select("SELECT * FROM $this->table WHERE $this->authTable = '". $authName ."';");
        if ($data) {
            if ($this->passwordVerify($password, $data[$this->passwordTable])) {
                return $data;
            }
        }
        return false;
    }

    public function register($authName, $password, $data)
    {

    }

    public function passwordEncrypt($password)
    {
        return password_hash($password, PASSWORD_BCRYPT, $this->options);
    }

    public function passwordVerify($password, $hash)
    {
        return password_verify($password, $hash);
    }

}