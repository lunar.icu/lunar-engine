<?php

class Session
{
    public function __construct()
    {
        session_start();
    }

    public function add($key, $value, $child = null)
    {
        if ($child) {
            $_SESSION[$key][$child] = $value;
        }
        $_SESSION[$key] = $value;

    }

    public function get($key, $child = null)
    {
        if ($child) {
            if (isset($_SESSION[$key][$child])) {
                return $_SESSION[$key][$child];
            }
            return ;
        } else {
            if (isset($_SESSION[$key])) {
                return $_SESSION[$key];
            }
        }
        
        return false;

    }

    public function delete($key, $child = null)
    {
        if ($child) {
            unset($_SESSION[$key][$child]);
        }
        unset($_SESSION[$key]);
    }

    public function printAll() {
        echo '<br><pre>';
        print_r($_SESSION);
        echo '</pre><br>';
    }
}